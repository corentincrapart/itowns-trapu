# Itowns-Trapu

Pour commencer on a trouvé un programme de conversion de ShapeFile en GeoJson.
[shapefile-js](https://github.com/calvinmetcalf/shapefile-js/tree/v3.4.3)

Pour faire fonctionner ce code on a généré un .cpg qui manquait dans les données Trapu.
Une fois utilisé dans un exemple iTowns on n'est pas arrivé à afficher le GeoJson.

Pour utiliser ce que l'on a produit il faut:
- mettre le fichier globe_wfs_extruded.html dans le dossier examples d'iTowns
- le dossier Trapu dans examples/layers
- shp.js dans examples/js
